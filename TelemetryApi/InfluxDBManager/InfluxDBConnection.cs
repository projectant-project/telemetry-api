﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InfluxDB.Client;

namespace TelemetryApi.InfluxDBManager
{
    public class InfluxDBConnection
    {
        private const string password = "root";
        private const string username = "root";
        private const string database = "telemetrydb";
        private const string retentionPolicy = "autogen";
        private const string measurement = "TelemetryData";

        public InfluxDBClient client { get; set; }

        public WriteApi writeApi { get; set; }

        public QueryApi queryApi { get; set; }

        private static InfluxDBConnection instance = null;

        private InfluxDBConnection()
        {
            this.client = InfluxDBClientFactory.CreateV1("http://localhost:8086",
                username,
                password.ToCharArray(),
                database,
                retentionPolicy);

            WriteOptions writeOptions = WriteOptions
                .CreateNew()
                .BatchSize(1000)
                .FlushInterval(1000)
                .Build();

            this.writeApi = client.GetWriteApi(writeOptions);
            this.queryApi = client.GetQueryApi();
        }

        public static InfluxDBConnection getInstance()
        {
            if (instance == null)
                instance = new InfluxDBConnection();

            return instance;
        }

        public string getDatabaseName() { return database; }

        public string getMeasurement() { return measurement; }

        public string getRetentionPolicy() { return retentionPolicy; }

        public void dispose() { this.client.Dispose(); }
    }
}
