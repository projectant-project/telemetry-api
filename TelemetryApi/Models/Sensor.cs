﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelemetryApi.Models
{
    public class Sensor
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
