﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelemetryApi.Models
{
    public class TelemetryFilter
    {
        public string Device { get; set; }
        public string Name { get; set; }
        public string Time { get; set; }
    }
}
